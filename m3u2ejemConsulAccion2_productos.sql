﻿/* Ejercicio 4 */
  -- crear campo nuevo DESCUENTO1 en la tabla
  ALTER TABLE productos ADD COLUMN descuento1 float;

  -- seleccion
  SELECT importe_base*0.1 descuento FROM productos WHERE rubro='Verduras';

  -- actualizacion
  UPDATE productos
    SET descuento1=importe_base*0.1
    WHERE rubro='Verduras';

  UPDATE productos
    SET descuento1=0
    WHERE rubro<>'Verduras';

  SELECT * FROM productos;


  /* Ejercicio 5 */
    -- crear campo nuevo DESCUENTO2 en la tabla
  ALTER TABLE productos ADD COLUMN descuento2 float;

    -- seleccion
    SELECT importe_base*0.2 FROM productos WHERE `u/medida`='atado';  -- productos atados

    SELECT importe_base*0.05 FROM productos WHERE `u/medida`<>'atado'; -- resto

    -- actualizacion
  UPDATE productos
    SET descuento2=importe_base*0.2
    WHERE `u/medida`='atado';

  UPDATE productos
    SET descuento2=importe_base*0.05
    WHERE `u/medida`<>'atado';

  SELECT * FROM productos;


/* Ejercicio 6 */
  -- crear campo nuevo DESCUENTO3 en la tabla
  ALTER TABLE productos ADD COLUMN descuento3 float;

  -- seleccion
  SELECT importe_base*0.2 FROM productos WHERE rubro='Frutas' AND importe_base>15;

  -- actualizacion
  UPDATE productos
    SET descuento3=importe_base*0.2
    WHERE rubro='Frutas' AND importe_base>15;

  UPDATE productos
    SET descuento3=0
    WHERE NOT (rubro='Frutas' AND importe_base>15);

  SELECT * FROM productos;


/* Ejercicio 7 */
  -- crear campo nuevo DESCUENTO4 en la tabla
  ALTER TABLE productos ADD COLUMN descuento4 float;

  -- seleccion
  SELECT importe_base*0.5 FROM productos WHERE granja='primavera' OR granja='litoral';

  -- actualizacion
  UPDATE productos
    SET descuento4=importe_base*0.5
    WHERE granja='primavera' OR granja='litoral';

  UPDATE productos
    SET descuento4=0
    WHERE NOT (granja='primavera' OR granja='litoral');

  SELECT * FROM productos;


  /* Ejercicio 8 */
  -- crear campo nuevo AUMENTO1 en la tabla
  ALTER TABLE productos ADD COLUMN aumento1 float DEFAULT 0;

  -- seleccion
    SELECT importe_base*0.1 FROM productos WHERE (rubro='frutas' OR rubro='verduras') AND (granja='la garota' OR granja='la pocha');

   -- actualizacion
  UPDATE productos
    SET aumento1=importe_base*0.1
    WHERE (rubro='frutas' OR rubro='verduras') AND (granja='la garota' OR granja='la pocha');

  SELECT * FROM productos p;


/* Ejercicio 9 */
    -- crear campo nuevo PRESENTACION en la tabla
  ALTER TABLE productos ADD COLUMN presentacion int;

    -- seleccion
    SELECT `u/medida` FROM productos WHERE `u/medida`='atado';  -- productos atados

    SELECT `u/medida` FROM productos WHERE `u/medida`='unidad';  -- productos unidad

    SELECT `u/medida` FROM productos WHERE `u/medida`='kilo';  -- productos kilo

    -- actualizacion
  UPDATE productos
    SET presentacion=1
    WHERE `u/medida`='atado';

  UPDATE productos
    SET presentacion=2
    WHERE `u/medida`='unidad';

  UPDATE productos
    SET presentacion=3
    WHERE `u/medida`='kilo';

  SELECT * FROM productos;


/* Ejercicio 10 */
    -- crear campo nuevo CATEGORIA en la tabla
  ALTER TABLE productos ADD COLUMN (categoria char(1));

    -- seleccion
    SELECT importe_base FROM productos WHERE importe_base<10;  -- productos <10

    SELECT importe_base FROM productos WHERE importe_base BETWEEN 10 AND 20;  -- productos entre 10 y 20 (inclusive)

    SELECT importe_base FROM productos WHERE importe_base>20;  -- productos >20

    -- actualizacion
  UPDATE productos
    SET categoria='A'
    WHERE importe_base<10;

  UPDATE productos
    SET categoria='B'
    WHERE importe_base BETWEEN 10 AND 20;

  UPDATE productos
    SET categoria='C'
    WHERE importe_base>20;

  SELECT * FROM productos;


/* Ejercicio 11 */
    -- crear campo nuevo AUMENTO2 en la tabla
  ALTER TABLE productos ADD COLUMN aumento2 float DEFAULT 0;

    -- seleccion
    SELECT importe_base*0.1 FROM productos WHERE rubro='frutas' AND granja='litoral';

    SELECT importe_base*0.15 FROM productos WHERE rubro='verduras' AND granja='el ceibal';

    SELECT importe_base*0.2 FROM productos WHERE rubro='semillas' AND granja='el canuto';

    -- actualizacion
  UPDATE productos
    SET aumento2=importe_base*0.1
    WHERE rubro='frutas' AND granja='litoral';

  UPDATE productos
    SET aumento2=importe_base*0.15
    WHERE rubro='verduras' AND granja='el ceibal';

  UPDATE productos
    SET aumento2=importe_base*0.2
    WHERE rubro='semillas' AND granja='el canuto';

  SELECT * FROM productos;